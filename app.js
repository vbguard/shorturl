const express = require("express");
const app = express();
const passport = require('passport');
const config = require("./config/config");
const apiRouter = require("./router/router");
const getUrl = require("./src/urls/getUrlById");

require("./db/connectionDB")();

if (config.mode === "development") {
  const logger = require("morgan");
  app.use(logger("dev"));
}

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const optionsReactViews = { beautify: true };

app.set("views", __dirname + "/views");
app.set("view engine", "jsx");
app.engine(
  "jsx",
  require("express-react-views").createEngine(optionsReactViews)
);
require('./middleware/passport')(passport);
app.use(passport.initialize());

// Шукати в БД існуючий шорт ід і по результату зробити редірект на той урл який знайдений
app.get("/:shortUrlId", getUrl);

// Віддати (зрендерити)  index.html  з формою для написання користувачем його урл з якого потрібно буде
// створити шорт урл і результам від отримає на цій же сторінці
// якщо у квері немає ніяких даних просто не промальовуємо відповідий ДОМ елемент
app.get("/", (req, res) => {
  const query = req.query;

  res.render("index", query);
});

app.get("/sign-in", (req, res) => {
  res.render("signIn");
});

app.use("/sign-up", (req, res) => {
  res.render("register");
});

app.use("/dashboard", (req, res) => {
  res.render("index");
});

//! API для роботи з урлами і користувачами
app.use("/api", apiRouter);

app.listen(config.port, () =>
  console.log(`Server running on port ${config.port} 🔥`)
);
