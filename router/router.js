const router = require("express").Router();
const { urlRouter } = require("../src/urls");
const { authRouter } = require("../src/auth");
const { usersRouter } = require("../src/users");

//TODO
//! 1. Auth
//! 2. Users
//! 3. urls by user
router.use("/url", urlRouter);
router.use("/users", usersRouter);
router.use("/auth", authRouter);
module.exports = router;
